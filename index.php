<html lang="ru" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>#Pets_Mall</title>
        <meta name="discription" content="">
        <meta name="keywords" content="наш сайт">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="fonts/Geometria/stylesheet.css">
        <link rel="stylesheet" type="text/css" href="js/slick/slick/slick.css"> 
		<link rel="stylesheet" type="text/css" href="js/slick/slick/slick-theme.css"> 

        <script type="text/javascript" src="js/jquery-3.4.1.js"></script> 
        <script type="text/javascript" src="js/slick/slick/slick.min.js"></script>
        <script type="text/javascript" src="js/slick/slick/slick.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
    </head>
    <body>
        <header class="header_pets">
            <div class="header_pets-menu">
                <div href="#" class="header_pets-log"> </div>
                <div class="header_pets-menu_block">
                    <a href="#aboutUs">О нас</a>
                    <a href="#chiots">Щенки</a>
                    <a href="#laureat">Победители</a>
                    <a href="#acheter">Купить</a>
                    <a href="#rapports">Контакты</a>
                </div>
            </div>
            <h2 class="header_pets-welcom">
                Добро пожаловать на
            </h2>
            <h1 class="header_pets-title">
                Pets Mall
            </h1>
            <p class="header_pets-text">11 чемпионов мира</p>
        </header>
        <section class="pets__about_us" id="aboutUs">
            <h2 class="pets__about_us-title">О нас</h2>
            <p class="pets__about_us-text">
                С 2000 года мы заботимся о питомцах в г. Москва. Наша мотивация - любовь к животным, 
                поэтому то, что мы и делаем - это не работа, а призвание. Все годы существование мы 
                старались расширять одновременно и клиентскую базу, и спектр оказываемых услуг, поэтому 
                сегодня мы с уверенностью можем сказать, что "собаки" - одно из лучших мест для ваших питомцев.
            </p>
        </section>
        <section id="laureat" class="pets__slider">
            <h2 class="pets__about_us-title">Победители</h2>
            <div class="autoplay">
                <img src="img/priz2.jpg" alt="">
                <img src="img/priz4.jpg" alt="">
            </div>
        </section>
        <section id="acheter">
            
        </section>
        
        <section class="chiots" id="chiots">
        <h2 class="pets__about_us-title">Щенки</h2>
        <ul class="pets__list">
        <?php
            require_once 'connection.php'; // подключаем скрипт
            
            // подключаемся к серверу
            $link = mysqli_connect($host, $user, $password, $database) 
                or die("Ошибка " . mysqli_error($link));
            
            // выполняем операции с базой данных
            $query ="SELECT * FROM chiots";
            $sql = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 
            if($sql)
            {
                //echo "Выполнение запроса прошло успешно\n";
                while ($result = mysqli_fetch_array($sql))
                {
                
                echo "<li><p class=\"breed\">".$result[1]."</p>";
                echo "<br>";
                echo "<p class=\"aboutC\">".$result[3]."</p>";
                echo "<br>";
                echo "<p class=\"price\">".$result[2]." руб.</p>";
                echo "<br>";
                echo"<div class=\"imgDiv\"><img src=\"img/chiots/".$result[0].".jpg\" class=\"imgList\"></div></li>";
                }
            }
            // закрываем подключение
            mysqli_close($link);
            ?>

        </ul>
            
        </section>


        <footer id="rapports">
            <section >
                <h2>Pets mall</h2>
                    <div>Липецк, ул. Советская, д.66</div>
                    <div>+7 4742 00-00-00</div>
            </section>
        </footer>
    </body>
</html>