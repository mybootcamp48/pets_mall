-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 15 2019 г., 16:31
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `pets_mall`
--

-- --------------------------------------------------------

--
-- Структура таблицы `chiots`
--

CREATE TABLE `chiots` (
  `id` int(11) NOT NULL,
  `breed` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `chiots`
--

INSERT INTO `chiots` (`id`, `breed`, `price`, `about`) VALUES
(0, 'Такса', 1000, 'Ius dicat feugiat no, vix cu modo dicat principes. Tation delenit percipitur at vix. An nam debet instructior, commodo mediocrem id cum. Eu eam dolores lobortis percipitur, quo te equidem deleniti disputando. Ceteros assentior omittantur cum ad.'),
(1, 'Хаски', 1000, 'Ius dicat feugiat no, vix cu modo dicat principes. Tation delenit percipitur at vix. An nam debet instructior, commodo mediocrem id cum. Eu eam dolores lobortis percipitur, quo te equidem deleniti disputando. Ceteros assentior omittantur cum ad.');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `chiots`
--
ALTER TABLE `chiots`
  ADD UNIQUE KEY `id` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
